/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service;

import com.easyaccounting.enitity.Activity;
import com.easyaccounting.enitity.Member;
import com.easyaccounting.enitity.Trip;
import com.easyaccounting.exception.EasyAccountingException;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Danny Chen
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class ActivityServiceImplTest {

    private static final Logger LOG = Logger.getLogger(ActivityServiceImplTest.class.getName());
    
    
    @Autowired
    private ActivityService activityServiceImpl;
    
    @Autowired
    private MemberService memberService;
    
    @Autowired
    private TripService tripService;
    
    public ActivityServiceImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void insertActivity() throws EasyAccountingException {
         //Danny
//         Member danny = memberService.getByEmail("danny19891219@gmail.com").get();
//         Trip trip = tripService.get(3).get();
//         LOG.info("member :" + danny);
//         LOG.info("trip" + trip);
//         Activity a = new Activity();
//         a.setName("住宿");
//         a.setDesc("Leo先墊");
//         a.setStatus(Activity.Status.PROCESSING);
//         
//         activityServiceImpl.createActivity(a, trip, danny);
//         
//         LOG.info("insert done!");
//         Assert.assertNotNull(a.getId());
     }
}
