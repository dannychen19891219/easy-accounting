/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.enitity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * 付款單
 *
 * @author Danny Chen
 */
@Entity
@Table(name = "payment")
public class Payment implements Serializable {

    /**
     * 付款序號
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    /**
     * 活動序號
     */
    @Column(name = "activity_id")
    private int activityID;

    /**
     * 付款明細
     */
    private String subject;

    /**
     * 備註
     */
    private String memo;

    /**
     * 付款日
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 付款金額
     */
    private BigDecimal price;

    /**
     * 付款明細
     */
    @Transient
    private List<PaymentDetail> paymentDetails;

    /**
     * 狀態
     */
    private int status;

    /**
     * 付款單狀態
     */
    public enum Status {
        INIT(0, "進行中"),
        END(1, "已結算"),
        DELETED(10, "已刪除"),
        UNDEFINED(99, "未定義");

        private final int value;

        private final String desc;

        private Status(int value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public int getValue() {
            return this.value;
        }

        public String getDesc() {
            return this.desc;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivityID() {
        return activityID;
    }

    public void setActivityID(int activityID) {
        this.activityID = activityID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<PaymentDetail> getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(List<PaymentDetail> paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public int getStatus() {
        return status;
    }

    public Status getStatusEnum() {
        Status[] statuses = Status.values();
        for (Status s : statuses) {
            if (this.status == s.getValue()) {
                return s;
            }
        }
        return Status.UNDEFINED;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(Status status) {
        this.status = status.getValue();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Payment other = (Payment) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
