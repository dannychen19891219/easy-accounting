/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.enitity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * 付款通知
 *
 * @author Danny Chen
 */
@Entity
@Table(name = "payment_notification")
public class PaymentNotification implements Serializable {

    /**
     * 序號
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    /**
     * 活動序號
     */
    @Column(name = "activity_id")
    private int activityID;

    /**
     * 狀態
     */
    private int status;

    /**
     * 被通知者
     */
    @Column(name = "participant_id")
    private int participantID;

    /**
     * 於活動中的預付款
     */
    private BigDecimal prepay;

    /**
     * 活動中總消費
     */
    private BigDecimal consume;

    /**
     * 創造通知日
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 付款確認日
     */
    @Column(name = "confirm_date")
    private Date confirmDate;

    /**
     * 備註
     */
    private String memo;

    /**
     * 狀態
     */
    public enum Status {
        INFORM(0, "通知中"),
        APPLIED(1, "審核中"),
        CONFIRM(2, "已確認結清"),
        DELETED(10, "已刪除"),
        UNDEFINED(99, "未定義");

        private final int value;

        private final String desc;

        private Status(int value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public int getValue() {
            return this.value;
        }

        public String getDesc() {
            return this.desc;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivityID() {
        return activityID;
    }

    public void setActivityID(int activityID) {
        this.activityID = activityID;
    }

    public int getStatus() {
        return status;
    }

    public Status getStatusEnum() {
        Status[] statuses = Status.values();
        for (Status s : statuses) {
            if (this.status == s.getValue()) {
                return s;
            }
        }

        return Status.UNDEFINED;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(Status status) {
        this.status = status.getValue();
    }

    public int getParticipantID() {
        return participantID;
    }

    public void setParticipantID(int participantID) {
        this.participantID = participantID;
    }

    public BigDecimal getPrepay() {
        return prepay;
    }

    public void setPrepay(BigDecimal prepay) {
        this.prepay = prepay;
    }

    public BigDecimal getConsume() {
        return consume;
    }

    public void setConsume(BigDecimal consume) {
        this.consume = consume;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaymentNotification other = (PaymentNotification) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
