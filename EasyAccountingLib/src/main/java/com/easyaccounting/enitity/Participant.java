/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.enitity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 活動人員
 *
 * @author Danny Chen
 */
@Entity
@Table(name = "participant")
public class Participant implements Serializable {

    /**
     * 活動者序號
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    /**
     * 活動序號
     */
    @Column(name = "trip_id")
    private int tripID;

    /**
     * 會員ID
     */
    @Column(name = "member_id")
    private int memberID;

    /**
     * 狀態
     */
    private int status;

    /**
     * 名稱
     */
    private String name;

    /**
     * 活動中，總開銷
     */
    @Column(name = "total_account")
    private BigDecimal totalAccount;

    /**
     * 活動中，總預付款
     */
    @Column(name = "total_prepay")
    private BigDecimal totalPrepay;

    /**
     * 加入活動日期
     */
    @Column(name = "join_date")
    private Date joinDate;

    /**
     * 備註
     */
    private String memo;

    /**
     * 參與者狀態
     */
    public enum Status {
        APPLIED(0, "審核中"),
        NORMAL(1, "已加入"),
        SUSPENDED(2, "停權"),
        DELETED(10, "已刪除"),
        UNDEFINED(99, "未定義");

        private final int value;

        private final String desc;

        private Status(int value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public int getValue() {
            return this.value;
        }

        public String getDesc() {
            return this.desc;
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public int getStatus() {
        return status;
    }

    public Status getStatusEnum() {
        Status[] statuses = Status.values();

        for (Status s : statuses) {
            if (this.status == s.getValue()) {
                return s;
            }
        }

        return Status.UNDEFINED;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(Status status) {
        this.status = status.getValue();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalAccount() {
        return totalAccount;
    }

    public void setTotalAccount(BigDecimal totalAccount) {
        this.totalAccount = totalAccount;
    }

    public BigDecimal getTotalPrepay() {
        return totalPrepay;
    }

    public void setTotalPrepay(BigDecimal totalPrepaid) {
        this.totalPrepay = totalPrepaid;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participant other = (Participant) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
