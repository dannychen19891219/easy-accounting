/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.enitity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * 活動單
 *
 * @author Danny Chen
 */
@Entity
public class Activity implements Serializable {

    /**
     * 序號
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "trip_id")
    private int tripID;

    /**
     * 活動名稱
     */
    private String name;

    /**
     * 簡述
     */
    @Column(name = "description")
    private String desc;

    /**
     * 活動主ID
     */
    @Column(name = "host_id")
    private int hostID;

    /**
     * 總計開銷
     */
    @Column(name = "total_account")
    private BigDecimal totalAccount;

    /**
     * 活動狀態
     */
    private int status;

    /**
     * 活動團員: 與Trip成員相同
     */
    @Transient
    private List<Participant> activityMembers;

    /**
     * 付款單
     */
    @Transient
    private List<Payment> payments;

    /**
     * 付款通知
     */
    @Transient
    private List<PaymentNotification> paymentNotifications;

    /**
     * 截止日
     */
    @Column(name = "end_date")
    private Date endDate;

    /**
     * 創造日
     */
    @Column(name = "create_date")
    private Date createDate;

    /**
     * 活動狀態
     */
    public enum Status {
        PROCESSING(0, "進行中"),
        END(1, "已截止"),
        CHECK(2, "已結單"),
        PAY_INFORM(3, "已發送付款通知"),
        DELETED(10, "已刪除"),
        UNDEFINED(99, "未定義");

        private final int value;

        private final String desc;

        private Status(int value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public int getValue() {
            return this.value;
        }

        public String getDesc() {
            return this.desc;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTripID() {
        return tripID;
    }

    public void setTripID(int tripID) {
        this.tripID = tripID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getHostID() {
        return hostID;
    }

    public void setHostID(int hostID) {
        this.hostID = hostID;
    }

    public BigDecimal getTotalAccount() {
        return totalAccount;
    }

    public void setTotalAccount(BigDecimal totalAccount) {
        this.totalAccount = totalAccount;
    }

    public int getStatus() {
        return status;
    }

    public Status getStatusEnum() {
        Status[] statuses = Status.values();
        for (Status s : statuses) {
            if (this.status == s.getValue()) {
                return s;
            }
        }

        return Status.UNDEFINED;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatus(Status status) {
        this.status = status.getValue();
    }

    public List<Participant> getActivityMembers() {
        return activityMembers;
    }

    public void setActivityMembers(List<Participant> activityMembers) {
        this.activityMembers = activityMembers;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<PaymentNotification> getPaymentNotifications() {
        return paymentNotifications;
    }

    public void setPaymentNotifications(List<PaymentNotification> paymentNotifications) {
        this.paymentNotifications = paymentNotifications;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Activity other = (Activity) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
