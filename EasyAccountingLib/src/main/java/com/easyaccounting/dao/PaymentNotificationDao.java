/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao;

import com.easyaccounting.enitity.PaymentNotification;

/**
 *
 * @author Danny Chen
 */
public interface PaymentNotificationDao extends EasyAccountingDaoInterface<PaymentNotification>{
    
}
