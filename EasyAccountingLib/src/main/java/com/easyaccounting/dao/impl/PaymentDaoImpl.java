/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao.impl;

import com.easyaccounting.dao.PaymentDao;
import com.easyaccounting.enitity.Payment;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danny Chen
 */
@Repository
public class PaymentDaoImpl implements PaymentDao {

  
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(Payment p) {
        entityManager.persist(p);
    }

    @Override
    public Optional<Payment> get(int id) {
        return Optional.ofNullable(entityManager.find(Payment.class, id));
    }

    @Override
    public Payment update(Payment p) {
        return entityManager.merge(p);
    }

    @Override
    public void delete(Payment p) {
        p.setStatus(Payment.Status.DELETED);
        entityManager.merge(p);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public void clear() {
        entityManager.clear();
    }

}
