/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao.impl;

import com.easyaccounting.dao.ParticipantDao;
import com.easyaccounting.enitity.Participant;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danny Chen
 */
@Repository
public class ParticipantDaoImpl implements ParticipantDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(Participant p) {
        entityManager.persist(p);
    }

    @Override
    public Optional<Participant> get(int id) {
        return Optional.ofNullable(entityManager.find(Participant.class, id));
    }

    @Override
    public Participant update(Participant p) {
        return entityManager.merge(p);
    }

    @Override
    public void delete(Participant p) {
        p.setStatus(Participant.Status.DELETED);
        entityManager.merge(p);
    }

    @Override
    public Optional<Participant> selectByMemberIDAndTripID(int memberID, int tripID) {
        TypedQuery<Participant> q = entityManager.createQuery("select p from Participant p "
                + "where p.tripID = :tripID AND p.memberID = :memberID", Participant.class);
        q.setParameter("tripID", tripID);
        q.setParameter("memberID", memberID);
        Participant p = null;
        try {
            p = q.getSingleResult();
        } catch (NoResultException ex) {
            p = null;
        }
        return Optional.ofNullable(p);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public void clear() {
        entityManager.clear();
    }

}
