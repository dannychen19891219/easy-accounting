/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao.impl;

import com.easyaccounting.dao.PaymentDetailDao;
import com.easyaccounting.enitity.PaymentDetail;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danny Chen
 */
@Repository
public class PaymentDetailDaoImpl implements PaymentDetailDao{
    
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(PaymentDetail p) {
        entityManager.persist(p);
    }

    @Override
    public Optional<PaymentDetail> get(int id) {
        return Optional.ofNullable(entityManager.find(PaymentDetail.class, id));
    }

    @Override
    public PaymentDetail update(PaymentDetail p) {
        return entityManager.merge(p);
    }

    @Override
    public void delete(PaymentDetail p) {
        entityManager.remove(p);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public void clear() {
        entityManager.clear();
    }
}
