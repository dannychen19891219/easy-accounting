/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao.impl;

import com.easyaccounting.dao.ActivityDao;
import com.easyaccounting.enitity.Activity;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danny Chen
 */
@Repository
public class ActivityDaoImpl implements ActivityDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(Activity a) {
        entityManager.persist(a);
    }

    @Override
    public Optional<Activity> get(int id) {
        return Optional.ofNullable(entityManager.find(Activity.class, id));
    }

    @Override
    public Activity update(Activity a) {
        return entityManager.merge(a);
    }

    @Override
    public void delete(Activity a) {
        a.setStatus(Activity.Status.DELETED);
        entityManager.merge(a);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public void clear() {
        entityManager.clear();
    }
    

}
