/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao.impl;

import com.easyaccounting.dao.PaymentNotificationDao;
import com.easyaccounting.enitity.PaymentNotification;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danny Chen
 */
@Repository
public class PaymentNotificationDaoImpl implements PaymentNotificationDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(PaymentNotification p) {
        entityManager.persist(p);
    }

    @Override
    public Optional<PaymentNotification> get(int id) {
        return Optional.ofNullable(entityManager.find(PaymentNotification.class, id));
    }

    @Override
    public PaymentNotification update(PaymentNotification p) {
        return entityManager.merge(p);
    }

    @Override
    public void delete(PaymentNotification p) {
        p.setStatus(PaymentNotification.Status.DELETED);
        entityManager.merge(p);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public void clear() {
        entityManager.clear();
    }
}
