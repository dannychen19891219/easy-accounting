/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao.impl;

import com.easyaccounting.dao.TripDao;
import com.easyaccounting.enitity.Trip;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danny Chen
 */
@Repository
public class TripDaoImpl implements TripDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void insert(Trip t) {
        entityManager.persist(t);
    }

    @Override
    public Optional<Trip> get(int id) {
        Optional<Trip> t = Optional.ofNullable(entityManager.find(Trip.class, id));
        return t;
    }

    @Override
    public Trip update(Trip t) {
        entityManager.merge(t);
        return t;
    }

    @Override
    public void delete(Trip t) {
        t.setStatus(Trip.Status.DELETED);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public void clear() {
        entityManager.clear();
    }

}
