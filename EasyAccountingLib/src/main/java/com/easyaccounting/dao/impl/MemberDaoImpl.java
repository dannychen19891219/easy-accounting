/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao.impl;

import com.easyaccounting.dao.MemberDao;
import com.easyaccounting.enitity.Member;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Danny chen
 */
@Repository
public class MemberDaoImpl implements MemberDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void insert(Member m) {
        entityManager.persist(m);
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public Optional<Member> get(int id) {
        return Optional.ofNullable(entityManager.find(Member.class, id));
    }

    @Override
    public Member update(Member m) {
        m = entityManager.merge(m);
        return m;
    }

    @Override
    public void delete(Member m) {
        m.setStatus(Member.Status.DELETE);
        entityManager.merge(m);
    }

    @Override
    public Optional<Member> selectByEmail(String email) {
        TypedQuery<Member> q = entityManager.createQuery("select m from Member m where m.email = :email", Member.class);
        q.setParameter("email", email);
        Member m = null;
        try {
            m = q.getSingleResult();
        } catch (NoResultException ex) {
            m = null;
        }
        return Optional.ofNullable(m);
    }

    @Override
    public void clear() {
        entityManager.clear();
    }

    @Override
    public Optional<Member> selectByAccountAndPwd(String email, String pwd) {
       TypedQuery<Member> q = entityManager.createQuery("select m from Member m where m.email = :email AND m.password = :pwd", Member.class);
        q.setParameter("email", email);
        q.setParameter("pwd", pwd);
        Member m = null;
        try {
            m = q.getSingleResult();
        } catch (NoResultException ex) {
            m = null;
        }
        return Optional.ofNullable(m);
    }

}
