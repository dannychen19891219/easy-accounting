/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao;

import com.easyaccounting.enitity.Member;
import java.util.Optional;

/**
 *
 * @author yam
 */
public interface MemberDao extends EasyAccountingDaoInterface<Member> {

    Optional<Member> selectByEmail(String email);

    public Optional<Member> selectByAccountAndPwd(String email, String pwd);
}
