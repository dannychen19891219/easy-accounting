/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.dao;

import java.util.Optional;

/**
 * 介面
 * @author Danny Chen
 */
public interface EasyAccountingDaoInterface<T> {
    
    void insert(T t);
    
    Optional<T> get(int id);
    
    T update(T t);
    
    void delete(T t);
    
    void flush();
    
    void clear();
}
