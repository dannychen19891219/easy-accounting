/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service;

import com.easyaccounting.enitity.Member;
import com.easyaccounting.enitity.Participant;
import com.easyaccounting.enitity.Trip;
import com.easyaccounting.exception.EasyAccountingException;
import java.util.Optional;

/**
 *
 * @author Danny Chen
 */
public interface ParticipantService {

    boolean checkParticipantRequiredField(Participant p);

    void create(Participant p, Member m, Trip t) throws EasyAccountingException;

    void delete(Participant t);

    Optional<Participant> get(int id);

    void insert(Participant p) throws EasyAccountingException;

    Participant update(Participant t);

    Optional<Participant> selectByMemberIDAndTripID(int memberID, int tripID);

    public void createAnonymousParticipant(Participant p, Trip t) throws EasyAccountingException;

}
