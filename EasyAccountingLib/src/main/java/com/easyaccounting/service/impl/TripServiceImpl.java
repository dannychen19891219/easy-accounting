/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service.impl;

import com.easyaccounting.service.TripService;
import com.easyaccounting.service.MemberService;
import com.easyaccounting.dao.TripDao;
import com.easyaccounting.enitity.Member;
import com.easyaccounting.enitity.Participant;
import com.easyaccounting.enitity.Trip;
import com.easyaccounting.exception.EasyAccountingException;
import com.easyaccounting.exception.ErrorCode;
import com.easyaccounting.service.ParticipantService;
import java.util.Date;
import java.util.Optional;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Danny Chen
 */
@Service
public class TripServiceImpl implements TripService {

    @Autowired
    private TripDao tripDao;
    @Autowired
    private MemberService memberService;
    @Autowired
    private ParticipantService participantService;

    @Transactional
    @Override
    public void insert(Trip t) throws EasyAccountingException {
        if (t == null) {
            throw new EasyAccountingException("Trip can not be null", ErrorCode.NULL_DATA);
        }
        if (checkTripRequiredField(t)) {
            int hostID = t.getHostID();
            Optional<Member> m = memberService.get(hostID);
            if (m.isPresent()) {
                tripDao.insert(t);
            } else {
                throw new EasyAccountingException("Member :" + t, ErrorCode.DATA_NOT_COMPLETE);
            }
        } else {
            throw new EasyAccountingException("Member :" + t, ErrorCode.DATA_NOT_COMPLETE);
        }

    }

    public void createTrip(Trip t, Member m) throws EasyAccountingException {
        if (m == null) {
            throw new EasyAccountingException("Member can not be null", ErrorCode.NULL_DATA);
        }
        int hostID = m.getId();
        t.setHostID(hostID);
        //建立活動
        insert(t);
        //把創建人加入自己的活動中
        join(t, m);
    }

    @Override
    public Optional<Trip> get(int id) {
        return tripDao.get(id);
    }

    @Override
    public Trip update(Trip t) throws EasyAccountingException {
        if (t != null) {
            return tripDao.update(t);
        } else {
            throw new EasyAccountingException("Trip can not be null", ErrorCode.NULL_DATA);
        }

    }

    /**
     * 回傳參與者資料
     *
     * @param t 要加入的旅程
     * @param m 要加入的會員
     * @return
     * @throws EasyAccountingException
     */
    @Override
    @Transactional
    public Participant join(Trip t, Member m) throws EasyAccountingException {
        if (t == null || m == null) {
            throw new EasyAccountingException("Trip or Member can not be null", ErrorCode.NULL_DATA);
        }
        Participant p = new Participant();
        p.setMemberID(m.getId());
        p.setTripID(t.getId());
        p.setName(StringUtils.isNotBlank(m.getNickName()) ? m.getNickName() : m.getLastName());
        p.setJoinDate(new Date());
        p.setStatus(Participant.Status.APPLIED);

        participantService.create(p, m, t);

        return p;

    }

    public Participant anonymousJoin(Trip t) throws EasyAccountingException {
        if (t == null ) {
            throw new EasyAccountingException("Trip or Member can not be null", ErrorCode.NULL_DATA);
        }
        Participant p = new Participant();
        p.setMemberID(0);
        p.setTripID(t.getId());
        p.setName("");
        p.setJoinDate(new Date());
        p.setStatus(Participant.Status.APPLIED);
        
        participantService.createAnonymousParticipant(p, t);
        return p;
    }

    @Override
    public void delete(Trip t) throws EasyAccountingException {
        if (t != null) {
            tripDao.delete(t);
        } else {
            throw new EasyAccountingException(ErrorCode.NULL_DATA);
        }
    }

    @Override
    public boolean checkTripRequiredField(Trip t) {
        String subject = t.getSubject();
        int hostID = t.getHostID();
        if (StringUtils.isBlank(subject)) {
            return false;
        }

        if (hostID <= 0) {
            return false;
        }

        return true;
    }

}
