/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service.impl;

import com.easyaccounting.service.ActivityService;
import com.easyaccounting.dao.ActivityDao;
import com.easyaccounting.enitity.Activity;
import com.easyaccounting.enitity.Member;
import com.easyaccounting.enitity.Participant;
import com.easyaccounting.enitity.Trip;
import com.easyaccounting.exception.EasyAccountingException;
import com.easyaccounting.exception.ErrorCode;
import com.easyaccounting.service.ParticipantService;
import java.util.Optional;
import javax.transaction.Transactional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Danny Chen
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private ParticipantService participantService;

    @Override
    public void insert(Activity a) throws EasyAccountingException {
        if (a != null) {
            if (checkActivityRequiredField(a)) {
                activityDao.insert(a);
            } else {
                throw new EasyAccountingException("Activity :" + a, ErrorCode.DATA_NOT_COMPLETE);
            }
        } else {
            throw new EasyAccountingException(ErrorCode.NULL_DATA);
        }
    }

    @Transactional
    @Override
    public void createActivity(Activity a, Trip t, Member m) throws EasyAccountingException {
        if (t == null || m == null) {
            throw new EasyAccountingException("activity or member can not be null", ErrorCode.NULL_DATA);
        }
        int tripID = t.getId();
        int hostID = m.getId();
        Optional<Participant> p = participantService.selectByMemberIDAndTripID(hostID, tripID);
        if (!p.isPresent()) {
            throw new EasyAccountingException("Member:"+ m .getId() +" is not in this TRIP", ErrorCode.NOT_ALLOWED);
        }
        a.setTripID(tripID);
        a.setHostID(hostID);
        insert(a);
    }

    @Override
    public Optional<Activity> get(int id) {
        return activityDao.get(id);
    }

    @Override
    public Activity update(Activity t) throws EasyAccountingException {
        if (t != null) {
            return activityDao.update(t);
        } else {
            throw new EasyAccountingException(ErrorCode.NULL_DATA);
        }
    }

    @Override
    public void delete(Activity t) {
        activityDao.delete(t);
    }

    @Override
    public boolean checkActivityRequiredField(Activity a) {
        //required field
        String name = a.getName();
        int hostID = a.getHostID();
        int tripID = a.getTripID();

        if (StringUtils.isBlank(name)) {
            return false;
        }

        if (hostID == 0 && tripID == 0) {
            return false;
        }
        return true;
    }

}
