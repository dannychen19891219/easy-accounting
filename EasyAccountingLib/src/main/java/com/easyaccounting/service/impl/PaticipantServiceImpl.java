/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service.impl;

import com.easyaccounting.service.ParticipantService;
import com.easyaccounting.dao.ParticipantDao;
import com.easyaccounting.enitity.Member;
import com.easyaccounting.enitity.Participant;
import com.easyaccounting.enitity.Trip;
import com.easyaccounting.exception.EasyAccountingException;
import com.easyaccounting.exception.ErrorCode;
import java.util.Optional;
import javax.persistence.NoResultException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Danny Chen
 */
@Service
public class PaticipantServiceImpl implements ParticipantService {

    @Autowired
    private ParticipantDao participantDao;

    @Override
    public void insert(Participant p) throws EasyAccountingException {
        if (p == null) {
            throw new EasyAccountingException("Participant can not be null", ErrorCode.NULL_DATA);
        }

        if (checkParticipantRequiredField(p)) {
            participantDao.insert(p);
        } else {
            throw new EasyAccountingException("Participant :" + p, ErrorCode.DATA_NOT_COMPLETE);
        }

    }

    @Override
    public Optional<Participant> get(int id) {
        return participantDao.get(id);
    }

    @Override
    public Participant update(Participant t) {
        return participantDao.update(t);
    }

    @Override
    public void delete(Participant t) {
        participantDao.delete(t);
    }

    @Override
    public void create(Participant p, Member m, Trip t) throws EasyAccountingException {
        if (m == null || t == null) {
            throw new EasyAccountingException("Member or Trip can not be null", ErrorCode.NULL_DATA);
        }
        p.setMemberID(m.getId());
        p.setTripID(t.getId());

        insert(p);
    }

    @Override
    public Optional<Participant> selectByMemberIDAndTripID(int memberID, int tripID) {
        return participantDao.selectByMemberIDAndTripID(memberID, tripID);
    }

    @Override
    public void createAnonymousParticipant(Participant p, Trip t) throws EasyAccountingException {
        if (t == null) {
            throw new EasyAccountingException("Trip can not be null", ErrorCode.NULL_DATA);
        }
        p.setTripID(t.getId());

        participantDao.insert(p);
    }

    @Override
    public boolean checkParticipantRequiredField(Participant p) {
        String name = p.getName();
        int memberID = p.getMemberID();
        int tripID = p.getTripID();

        if (StringUtils.isBlank(name)) {
            return false;
        }

        if (memberID == 0 || tripID == 0) {
            return false;
        }
        return true;
    }

}
