/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service.impl;

import com.easyaccounting.service.MemberService;
import com.easyaccounting.dao.MemberDao;
import com.easyaccounting.enitity.Member;
import com.easyaccounting.exception.EasyAccountingException;
import com.easyaccounting.exception.ErrorCode;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Danny Chen
 */
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberDao memberDao;

    @Override
    public void insert(Member t) throws EasyAccountingException {
        //必填check
        if (this.checkMemberRequiredField(t)) {
            Optional<Member> m = memberDao.selectByEmail(t.getEmail());
            if (m.isPresent()) {
                throw new EasyAccountingException("Member : " + t,ErrorCode.DATA_NOT_COMPLETE);
            }
            memberDao.insert(t);
            return;

        } else {
            throw new EasyAccountingException(ErrorCode.DUPLICATE);
        }
    }

    @Override
    public Optional<Member> get(int id) {
        return memberDao.get(id);
    }

    @Override
    public Member update(Member t) throws EasyAccountingException {
        if (t != null) {
            memberDao.update(t);
            return t;
        } else {
            throw new EasyAccountingException(ErrorCode.DUPLICATE);
        }
    }

    @Override
    public void delete(Member t) throws EasyAccountingException{
        if (t != null) {
            memberDao.delete(t);
        } else {
            throw new EasyAccountingException(ErrorCode.DUPLICATE);
        }
    }

    @Override
    public Optional<Member> getByEmail(String email) {
        return memberDao.selectByEmail(email);
    }
    
        @Override
    public Optional<Member> login(String email, String pwd) {
        return memberDao.selectByAccountAndPwd(email,pwd);
    }

    

    @Override
    public  boolean checkMemberRequiredField(Member m) {
        //必填欄位
        String email = m.getEmail();
        String pwd = m.getPassword();
        String mobile = m.getMobile();
        String lastName = m.getLastName();
        String firstName = m.getFirstName();

        if (StringUtils.isBlank(email) || StringUtils.isBlank(pwd)) {
            return false;
        }

        if (StringUtils.isBlank(mobile)) {
            return false;
        }

        if (StringUtils.isBlank(lastName) || StringUtils.isBlank(firstName)) {
            return false;
        }

        return true;
    }
    
    

}
