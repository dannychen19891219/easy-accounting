/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service;

import com.easyaccounting.enitity.Member;
import com.easyaccounting.exception.EasyAccountingException;
import java.util.Optional;

/**
 *
 * @author Danny Chen
 */
public interface MemberService {

    boolean checkMemberRequiredField(Member m);

    void delete(Member t) throws EasyAccountingException;

    Optional<Member> get(int id);
    
    Optional<Member> getByEmail(String email);

    void insert(Member t) throws EasyAccountingException;

    Member update(Member t) throws EasyAccountingException;
    
    Optional<Member> login(String email,String pwd);
    
    
}
