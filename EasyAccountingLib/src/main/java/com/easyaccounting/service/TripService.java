/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service;

import com.easyaccounting.enitity.Member;
import com.easyaccounting.enitity.Participant;
import com.easyaccounting.enitity.Trip;
import com.easyaccounting.exception.EasyAccountingException;
import java.util.Optional;
import javax.transaction.Transactional;

/**
 *
 * @author Danny Chen
 */
public interface TripService {

    boolean checkTripRequiredField(Trip t);

    void delete(Trip t) throws EasyAccountingException;

    Optional<Trip> get(int id);

    @Transactional
    void insert(Trip t) throws EasyAccountingException;

    Trip update(Trip t) throws EasyAccountingException;
    
    Participant join(Trip t,Member m) throws EasyAccountingException;
    
}
