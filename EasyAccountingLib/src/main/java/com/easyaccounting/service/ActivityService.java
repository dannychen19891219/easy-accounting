/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.service;

import com.easyaccounting.enitity.Activity;
import com.easyaccounting.enitity.Member;
import com.easyaccounting.enitity.Trip;
import com.easyaccounting.exception.EasyAccountingException;
import java.util.Optional;
import javax.transaction.Transactional;

/**
 *
 * @author Danny Chen
 */
public interface ActivityService {

    boolean checkActivityRequiredField(Activity a);

    @Transactional
    void createActivity(Activity a, Trip t, Member m) throws EasyAccountingException;

    void delete(Activity t);

    Optional<Activity> get(int id);

    void insert(Activity a) throws EasyAccountingException;

    Activity update(Activity t) throws EasyAccountingException;
    
}
