/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.exception;

/**
 *
 * @author Danny Chen
 */
public class EasyAccountingException extends Exception {

    private ErrorCode errorCode;

    public EasyAccountingException(ErrorCode code) {
        super();
        this.errorCode = code;
    }

    public EasyAccountingException(String message, ErrorCode code) {
        super(message);
        this.errorCode = code;
    }

    public EasyAccountingException(String message, Throwable cause, ErrorCode code) {
        super(message, cause);
        this.errorCode = code;
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String toString() {
        return "EasyAccountingException{" + "errorCode=" + errorCode + '}';
    }
}
