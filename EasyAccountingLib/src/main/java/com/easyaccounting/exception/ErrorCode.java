/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.exception;

/**
 *
 * @author Danny Chen
 */
public enum ErrorCode {
    DATA_NOT_COMPLETE(1,"資料不齊全"),
    DUPLICATE(2,"資料重複"),
    NULL_DATA(3,"傳入物件為空"),
    NOT_ALLOWED(4,"無法執行");
    
    private int code;
    private String msg;
    private ErrorCode(int code,String msg){
        this.code = code;
        this.msg = msg;
    }
    
    public int getCode() {
        return this.code;
    }
    
    public String getMsg(){
        return this.msg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" + "code=" + code + ", msg=" + msg + '}';
    }
    
}
