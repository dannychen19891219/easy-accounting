<%-- 
    Document   : nav
    Created on : 2017/9/1, 下午 11:17:14
    Author     : Danny Chen
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}/">EZ-Accounting</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                    <c:choose>
                        <c:when test="${not empty sessionScope.member}">
                        <li><a href="#portfolio">活動列表</a></li>
                        <li><a href="${pageContext.request.contextPath}/member/profile">個人資料</a></li>
                        <li><a href="${pageContext.request.contextPath}/logout">登出</a></li>
                        </c:when>
                        <c:otherwise>
                        <li><a href="#about">關於</a></li>
                        <li><a href="#register">註冊</a></li>
                        </c:otherwise>
                    </c:choose>
            </ul>
        </div>
    </div>
</nav>
