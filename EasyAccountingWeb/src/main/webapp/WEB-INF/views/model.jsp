<%-- 
    Document   : model
    Created on : 2017/2/13, 下午 03:03:32
    Author     : yam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h1>
            ${autoIntoModel.ip}
        </h1>
        <h1>
            ${autoIntoModel.name}
        </h1>
        <h1>
            ${autoIntoModel.address}
        </h1>
    </body>
</html>
