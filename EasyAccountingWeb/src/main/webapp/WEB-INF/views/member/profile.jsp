<%-- 
    Document   : hello
    Created on : 2017/8/30, 下午 10:15:49
    Author     : Danny Chen
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="/WEB-INF/subviews/jsTemplate.jsp" %>
        <title>個人資料</title>
    </head>
    <body>
        <%@include file="/WEB-INF/subviews/nav.jsp" %>
        <div class="container-fluid bg-grey">
            <h2 class="text-center">
                <span>個人資料</span>
            </h2>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <c:set var="formAction" value="${empty sessionScope.member ?  '/register' : 'member/update'}"/>
                    <form name="updateMember" action="${pageContext.request.contextPath}${formAction}" >
                        <div class="row">
                            <input type="hidden" id="id" name="id" value="${sessionScope.member.id}"/>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">暱稱</label>
                                <input class="form-control" id="nickName" name="nickName" placeholder="nickName" type="text" value="${sessionScope.member.nickName}" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">E-Mail</label>
                                <input class="form-control" id="email" name="email" placeholder="Email" type="email"  value="${sessionScope.member.email}" ${empty sessionScope.member ? 'required' : 'readonly'}>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">姓</label>
                                <input class="form-control" id="lastName" name="lastName" placeholder="lastName" type="text" value="${sessionScope.member.lastName}" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">名</label>
                                <input class="form-control" id="firstName" name="firstName" placeholder="firstName" type="text" value="${sessionScope.member.firstName}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">Line ID</label>
                                <input class="form-control" id="lineAccount" name="lineAccount" placeholder="LineID" type="text" value="${sessionScope.member.lineAccount}" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">手機</label>
                                <input class="form-control" id="firstName" name="mobile" placeholder="手機" type="text"  value="${sessionScope.member.mobile}"required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <label class="control-label">銀行代碼</label>
                                <input class="form-control" id="bankID" name="bankID" placeholder="Bank ID" type="text" value="${sessionScope.member.bankID}" required>
                            </div>
                            <div class="col-sm-6 form-group">
                                <label class="control-label">帳戶帳號</label>
                                <input class="form-control" id="bankAccount" name="bankAccount" placeholder="Bank Account" type="text" value="${sessionScope.member.bankAccount}" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <button class="btn btn-default pull-right" type="submit">${empty sessionScope.member ? '註冊' : '更新資料'}</button>
                            </div>
                        </div> 
                    </form>
                </div>
                <div class="col-sm-2"></div>
            </div>
        </div>
    </body>
</html>
