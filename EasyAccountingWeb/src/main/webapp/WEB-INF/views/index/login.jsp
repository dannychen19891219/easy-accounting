<%-- 
    Document   : hello
    Created on : 2017/8/30, 下午 10:15:49
    Author     : Danny Chen
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="/WEB-INF/subviews/jsTemplate.jsp" %>
        <title>EZ-Accounting</title>
    </head>
    <body>
        <%@include file="/WEB-INF/subviews/nav.jsp" %>
        <div class="jumbotron text-center container-fluid">
            <h1>旅遊好幫手</h1> 
            <p>與朋友遊玩，不再停下腳步</p> 
            <c:if test="${loginErr && pageContext.request.method == 'POST'}" >
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    登入失敗，請重新登入喔 >.^
                </div>
            </c:if>
            <form class="form-inline" name="loginForm" method="POST" action="${pageContext.request.contextPath}/login">
                <div class="input-group">
                    <div class="input-group-btn">
                        <input type="text" class="form-control"  id="email" name="email" placeholder="請輸入Email" value="${param.email}" required="true">
                        <input type="password" class="form-control" id="pwd" name="pwd" placeholder="請輸入密碼" required="true"/>
                        <input type="submit" class="form-control" value="登入">
                    </div>
                </div>
            </form>
            <div> or </div>
            <form class="form-inline" name="registerForm" method="GET" action="">
                <div class="input-group">
                    <div class="input-group-btn">
                        <input type="submit" class="form-control" value="立即註冊">
                    </div>
                </div>
            </form>
        </div>

        <div id="about" class="container-fluid">
            <div class="row">
                <div class="col-sm-8">
                    <h2>更專注在活動的當下</h2>
                    <h4>不用再花心思釐清誰付了多少？而誰又應該要給多少？</h4> 
                    <h4>交給我們，你就會發現，原來活動的費用分攤如此簡易明瞭</h4> 
                </div>
                <div class="col-sm-4">
                    <img src="${pageContext.request.contextPath}/resources/images/IMAG0311.jpg" class="img-circle" style="width:100%"/>
                </div>
            </div>
        </div>

        <div class="container-fluid bg-grey">
            <div class="row">
                <div class="col-sm-4">
                    <img src="${pageContext.request.contextPath}/resources/images/IMAG0683.jpg" class="img-circle" style="width:100%" />
                </div>
                <div class="col-sm-8">
                    <h2>致力於讓活動費用資訊更明瞭</h2>
                    <h4>與朋友間的聚會，身爲主辦人，在最後結算費用的時候總是一個頭兩個大</h4> 
                    <h4>別再錯過留下寶貴回憶的機會，你可以把心思留在活動之中</h4>
                    <h4>費用就交給EZ-Accouting幫你與大家結算</h4>
                </div>
            </div>
        </div>

    </body>
</html>
