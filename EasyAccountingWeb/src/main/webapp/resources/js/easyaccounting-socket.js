/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function webSocketInit()
{
    if ("WebSocket" in window)
    {
        console.log("您的浏览器支持 WebSocket!");

        // 打开一个 web socket
        var ws = new WebSocket("ws://localhost:8080/socketServer");

        ws.onopen = function ()
        {
            // Web Socket 已连接上，使用 send() 方法发送数据
            ws.send("发送数据");
            console.log("数据发送中...");
        };

        ws.onmessage = function (evt)
        {
            var received_msg = evt.data;
            console.log("数据已接收...");
            console.log(evt);
        };

        ws.onclose = function ()
        {
            // 关闭 websocket
            console.log("连接已关闭...");
        };
    } else
    {
        // 浏览器不支持 WebSocket
        console.log("您的浏览器不支持 WebSocket!");
    }
}

function webPush() {
    if (Notification.permission == "granted") {
    var notification = new Notification("Hi，帅哥：", {
        body: '可以加你为好友吗？',
        icon: 'mm1.jpg'
    });
    
    notification.onclick = function() {
        notification.close();    
    };
}    
}

$(window).on('load',function(){
    webSocketInit();
});


