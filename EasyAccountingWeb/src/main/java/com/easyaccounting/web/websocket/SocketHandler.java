/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.web.websocket;

import com.easyaccounting.enitity.Member;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 *
 * Socket處理累
 * @author Danny Chen
 */
@Service
public class SocketHandler implements WebSocketHandler{
    
    private static final Map<Integer,WebSocketSession> memberWebSessions;
    
    static {
        memberWebSessions = new HashMap<>();
    }

    /**
     * 建立連線後第一步
     * @param wss
     * @throws Exception 
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession wss) throws Exception {
        System.out.println("成功建立socket連接");
        Member m = (Member) wss.getHandshakeAttributes().get("member");
        memberWebSessions.put(m.getId(), wss);
        wss.sendMessage(new TextMessage("建立Socket"));
    }

    @Override
    public void handleMessage(WebSocketSession wss, WebSocketMessage<?> wsm) throws Exception {
        System.out.println("hanlert Message");
    }

    @Override
    public void handleTransportError(WebSocketSession wss, Throwable thrwbl) throws Exception {
     if(wss.isOpen()){
         wss.close();;
     }
        System.out.println("連間出現錯誤" + thrwbl.getMessage());
        memberWebSessions.remove(wss);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession wss, CloseStatus cs) throws Exception {
        System.out.println("連線關閉");
        memberWebSessions.remove(wss);
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
    
    /**
     * 發給在線所有使用者
     * @param message 傳遞訊息
     */
    public void sendToAllUsers(TextMessage message){
        for(WebSocketSession session : memberWebSessions.values()){
            if(session.isOpen()){
                try {
                    session.sendMessage(message);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    public void sendToSpecificUsers(Set<Integer> userIds , TextMessage message){
        for(int userid : userIds){
            WebSocketSession session = memberWebSessions.get(userid);
            if(session != null && session.isOpen()){
                try {
                    session.sendMessage(message);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
}
