/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.web.controller;

import com.easyaccounting.enitity.Member;
import com.easyaccounting.service.MemberService;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author sweetpotato
 */
@Controller
public class IndexController {

    @Autowired
    private MemberService memberService;

    @RequestMapping(value = "/login")
    public String login(HttpServletRequest request, HttpSession session) {
        if (session.getAttribute("member") != null) {
            return "redirect:/member/profile";
        }
        String email = request.getParameter("email");
        String pwd = request.getParameter("pwd");
        if (StringUtils.isBlank(pwd) || StringUtils.isBlank(email)) {
            request.setAttribute("loginErr", true);
            return "index/login";
        }
        Optional<Member> m = memberService.login(email, pwd);
        if (m.isPresent()) {
            session.setAttribute("member", m.get());
            return "redirect:member/profile";
        } else {
            request.setAttribute("loginErr", true);
            return "index/login";
        }
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpSession session) {
        session.removeAttribute("member");
        return "index/login";
    }
}
