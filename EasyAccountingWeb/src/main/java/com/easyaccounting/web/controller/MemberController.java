/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.web.controller;

import com.easyaccounting.enitity.Member;
import com.easyaccounting.service.MemberService;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Danny Chen
 */
@Controller
@RequestMapping(value = "/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @RequestMapping(value = "/profile")
    public String profile(HttpServletRequest request) {
        return "member/profile";
    }

    @RequestMapping(value = "/update")
    public String update() {
        System.out.println("TEST GET M");
        return "member/profile";
    }


    public Member getUpdateMember(HttpServletRequest request) {
        Member m = new Member();
        m.setId(Integer.parseInt(request.getParameter("id")));
        m.setNickName(request.getParameter("nickName"));
        String name = request.getParameter("name");
        if (StringUtils.isNotBlank(name)) {
            String[] names = name.split(" ");
            m.setLastName(names[1]);
            m.setFirstName(names[2]);
        }
        m.setEmail(request.getParameter("email"));
        m.setId(Integer.parseInt(request.getParameter("id")));
        m.setMobile(request.getParameter("mobile"));
        m.setLineAccount(request.getParameter("lineAccount"));
        m.setBankID(request.getParameter("bankID"));
        m.setBankAccount(request.getParameter("bankAccount"));
        return m;
    }
}
