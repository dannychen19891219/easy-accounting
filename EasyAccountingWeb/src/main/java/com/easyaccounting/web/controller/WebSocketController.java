/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.easyaccounting.web.controller;

import com.easyaccounting.web.websocket.SocketHandler;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.socket.TextMessage;

/**
 *
 * @author Danny Chen
 */
@Controller
public class WebSocketController {
    
    @Autowired
    private SocketHandler socketHandler;
    
    @RequestMapping(value = "/message",method = RequestMethod.GET)
    public void sendMessage(HttpSession session){
        socketHandler.sendToAllUsers(new TextMessage("測試推播功能"));
    }
}
